$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("featurefile/login/login.feature");
formatter.feature({
  "line": 1,
  "name": "Login to application",
  "description": "",
  "id": "login-to-application",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3679475227,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Login to application",
  "description": "",
  "id": "login-to-application;login-to-application",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "navigate to application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user click on sign in link",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "enter email address as \"rohitd@360logica.com\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "enter password as \"360@Logica\"",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "click on sign in button",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Login is successful",
  "keyword": "Then "
});
formatter.match({
  "location": "LogintoApplication.navigate_to_application()"
});
formatter.result({
  "duration": 10318565157,
  "status": "passed"
});
formatter.match({
  "location": "LogintoApplication.user_click_on_sign_in_link()"
});
formatter.result({
  "duration": 3148718930,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "rohitd@360logica.com",
      "offset": 24
    }
  ],
  "location": "LogintoApplication.enter_email_address_as(String)"
});
formatter.result({
  "duration": 180991810,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "360@Logica",
      "offset": 19
    }
  ],
  "location": "LogintoApplication.enter_password_as(String)"
});
formatter.result({
  "duration": 109143077,
  "status": "passed"
});
formatter.match({
  "location": "LogintoApplication.click_on_sign_in_button()"
});
formatter.result({
  "duration": 2454383496,
  "status": "passed"
});
formatter.match({
  "location": "LogintoApplication.login_is_successful()"
});
formatter.result({
  "duration": 56123008,
  "status": "passed"
});
formatter.after({
  "duration": 348861,
  "status": "passed"
});
});