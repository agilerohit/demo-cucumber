package com.cucumber.runner;

import org.testng.annotations.Test;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = { "classpath:featurefile/createnewcontact" }, glue = {"classpath:com.cucumber.stepdefination"}, format = {"pretty","html:test-output","json:json_output/cucumber.json","junit:junit_xml/cucumber.xml"},
monochrome=true,
strict=true,
dryRun=false)

public class Runner extends AbstractTestNGCucumberTests{
 
}
