Feature: Deal data creation

Scenario: Free CRM Create a new deal scenario
Given user on Login Page
When name of page is Free CRM
Then user enters login credentials
|username| |password|
|rohittest| |rohittest|

Then user click on login button
Then user on home page
Then user moves to new deal page
Then user enters deal details
|title| amount | |probability | |commision|
|test deals1| |1000| |50| |10|
|test deals2| |2000| |60| |20|
|test deals3| |3000| |70| |30|

Then Close the browser



