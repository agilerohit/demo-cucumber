#Without Example Keyword
#Feature: Free CRM Application Test
#Scenario: Verify Free CRM Home Page Test
#Given user opens browser
#When user title of login page is Free CRM
#Then user enters "rohittest" and "rohittest"
#Then user clicks on login button
#Then user is on home page
#With Example Keyword
Feature: Free CRM Application Test
Scenario Outline: Verify Free CRM Home Page Test
Given user opens browser
When user title of login page is Free CRM
Then user enters "<username>" and "<password>"
Then user clicks on login button
Then user is on home page

Examples:

 |username| |password|
 |rohittest| |rohittest|
 


