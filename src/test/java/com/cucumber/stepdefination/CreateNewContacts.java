/*package com.cucumber.stepdefination;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.cucumber.PageObject.HomePageHelper;
import com.cucumber.PageObject.LoginPageHelper;
import com.cucumber.config.ObjectRepo;
import com.cucumber.helper.LoggerHelper;
import com.cucumber.helper.TestBase;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class CreateNewContacts  {
	
	LoginPageHelper loginPageHelper;
	HomePageHelper  homePageHelper;
	WebDriver driver;
		
	private final Logger log = LoggerHelper.getLogger(CreateNewContacts.class);

	
	@Given("^user open browser$")
	public void user_opens_browser() throws Throwable {
	TestBase.driver.get(ObjectRepo.reader.getWebsite());
	}

	@Then("^user enter \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enters_username_and_password(String username, String password) {
	loginPageHelper = new LoginPageHelper(TestBase.driver);
	loginPageHelper.login(username, password);

	}

	@Then("^user click on login button$")
	public void user_clicks_on_login_button() {
	loginPageHelper.clickOnLoginButton();

	}

	@Then("^click on Contacts on Home Page$")
	public void click_on_Contacts_on_Home_Page() throws Throwable {
		driver.switchTo().frame("mainpanel");
		Actions action = new Actions(driver);
		WebElement txt = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		action.moveToElement(txt).build().perform();
		Thread.sleep(3000);
		WebElement newcontxt = driver.findElement(By.xpath("//a[contains(text(),'New Contact')]"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", newcontxt);
		homePageHelper.clickOnNewContactLinkndCreateNewContact();

	}

	@Then("^user enters \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")

	public void user_enters_contacts_details(String firstname, String lastname, String position) {
		try{
		driver.findElement(By.id("first_name")).sendKeys(firstname);
		driver.findElement(By.id("surname")).sendKeys(lastname);
		driver.findElement(By.id("company_position")).sendKeys(position);
		driver.findElement(By.xpath("//input[@type='submit' and @value='Save']")).click();
		}
		
		catch(WebDriverException e) {
		e.getMessage();
			
		}
		homePageHelper.fillUserForm(firstname, lastname, position);
	}

}
*/