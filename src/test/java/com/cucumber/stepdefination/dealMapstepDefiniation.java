package com.cucumber.stepdefination;

import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.cucumber.PageObject.HomePageHelper;
import com.cucumber.PageObject.LoginPageHelper;
import com.cucumber.config.ObjectRepo;
import com.cucumber.helper.LoggerHelper;
import com.cucumber.helper.TestBase;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class dealMapstepDefiniation {

	LoginPageHelper loginPageHelper;
	HomePageHelper homePageHelper;
	WebDriver driver;

	private final Logger log = LoggerHelper.getLogger(LoginintocrmApplication.class);

	@Given("^user on Login Page$")
	public void user_opens_browser() throws Throwable {

		TestBase.driver.get(ObjectRepo.reader.getWebsite());
	}

	@When("^name of page is Free CRM$")
	public void user_title_of_login_page_is_Free_CRM() {
		loginPageHelper = new LoginPageHelper(TestBase.driver);
		loginPageHelper.validateLoginPageTitle();

	}

	@Then("^user enters login credentials$")
	public void user_enters_username_and_password(DataTable credentials) {
		for (Map<String, String> data : credentials.asMaps(String.class, String.class)) {
			loginPageHelper.login(data.get("username"), data.get("password"));
		}
	}

	@Then("^user click on login button$")
	public void user_clicks_on_login_button() {

		loginPageHelper.clickOnLoginButton();

	}

	@Then("^user on home page$")
	public void user_is_on_home_page() {

		homePageHelper.verifyHomeTitle();

	}

	@Then("^user moves to new deal page$")
	public void user_moves_to_new_deal_page() throws Throwable {
		homePageHelper.movestoDealPage();
	}

	@Then("^user enters deal details$")
	public void user_enters_deal_details(DataTable dealDetails) {
		for (Map<String, String> data : dealDetails.asMaps(String.class, String.class)) {
			homePageHelper.fillNewDealsForm(data.get("title"), data.get("amount"), data.get("probability"),
					data.get("commision"));

		}

	}

	@Then("^Close the browser$")
	public void close_the_browser() {
		driver.quit();

	}

}
