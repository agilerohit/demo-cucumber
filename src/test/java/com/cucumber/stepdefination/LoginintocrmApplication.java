package com.cucumber.stepdefination;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.cucumber.PageObject.HomePageHelper;
import com.cucumber.PageObject.LoginPageHelper;
import com.cucumber.config.ObjectRepo;
import com.cucumber.helper.LoggerHelper;
import com.cucumber.helper.TestBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginintocrmApplication {
	
	LoginPageHelper loginPageHelper;
	HomePageHelper  homePageHelper;
	WebDriver driver;
		
	private final Logger log = LoggerHelper.getLogger(LoginintocrmApplication.class);
		@Given("^user opens browser$")
		public void user_opens_browser() throws Throwable{
		
		TestBase.driver.get(ObjectRepo.reader.getWebsite());
		}

		@When("^user title of login page is Free CRM$")
		public void user_title_of_login_page_is_Free_CRM() {
		loginPageHelper= new LoginPageHelper(TestBase.driver);
		loginPageHelper.validateLoginPageTitle();
		  
		}

		@Then("^user enters \"([^\"]*)\" and \"([^\"]*)\"$")
		public void user_enters_username_and_password(String username, String password){
		loginPageHelper.login(username, password);
		
		}

		@Then("^user clicks on login button$")
		public void user_clicks_on_login_button() {
			
		loginPageHelper.clickOnLoginButton(); 
		    
		}

		@Then("^user is on home page$")
		public void user_is_on_home_page() {
		
		homePageHelper.verifyHomeTitle();
		
		}

		
		
	}	


