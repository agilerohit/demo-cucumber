package com.cucumber.stepdefination;

import org.apache.log4j.Logger;
import org.testng.Assert;

import com.cucumber.PageObject.LoginPage;
import com.cucumber.config.ObjectRepo;
import com.cucumber.helper.LoggerHelper;
import com.cucumber.helper.TestBase;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LogintoApplication {

private final Logger log = LoggerHelper.getLogger(LogintoApplication.class);

LoginPage loginPage;

	@Given("^navigate to application$")
	public void navigate_to_application() throws Throwable {
	TestBase.driver.get(ObjectRepo.reader.getWebsite());
	}

	@When("^user click on sign in link$")
	public void user_click_on_sign_in_link() throws Throwable {
	loginPage= new LoginPage(TestBase.driver);
	loginPage.clickOnSignInLink();
    }

	@When("^enter email address as \"([^\"]*)\"$")
	public void enter_email_address_as(String arg1) throws Throwable {
	loginPage.enterEmailAddress(arg1);
	}

	@When("^enter password as \"([^\"]*)\"$")
	public void enter_password_as(String arg1) throws Throwable {
	loginPage.enterPassword(arg1);
	}

	@When("^click on sign in button$")
	public void click_on_sign_in_button() throws Throwable {
	loginPage.clickOnSubmitButton();
	}

	@Then("^Login is successful$")
	public void login_is_successful() throws Throwable {
	if(loginPage.verifySuccessLoginMsg()){
	log.info("login test is pass");
	}
	else{
	Assert.assertTrue(false, this.getClass().getSimpleName()+" is fail");
	}
	}

	

}
