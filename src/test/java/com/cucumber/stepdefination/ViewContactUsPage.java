package com.cucumber.stepdefination;

import java.sql.Driver;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.cucumber.PageObject.ContactUsPage;
import com.cucumber.helper.LoggerHelper;
import com.cucumber.helper.TestBase;

import cucumber.api.java.en.Then;

public class ViewContactUsPage {
	
	private final Logger log = LoggerHelper.getLogger(ViewContactUsPage.class);
	ContactUsPage contactUsPage;
	
	@Then("^click on Contact us link$")
	public void click_on_Contact_us_link() throws Throwable {
	contactUsPage = new ContactUsPage(TestBase.driver);
	contactUsPage.clickOnContactUsLink();
	}

	@Then("^view Contact Us Page$")
	public void view_Contact_Us_Page() throws Throwable {
	log.info("View contact Us Page");
	}	

}
