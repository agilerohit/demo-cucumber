package com.cucumber.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

//@CucumberOptions(features = { "classpath:featurefile/login/login.feature" }, glue = {"classpath:com.cucumber.stepdefination", "classpath:com.cucumber.helper" }, plugin = {"html:target/cucumber-html-report"})
@CucumberOptions(features = { "classpath:featurefile/Tagging/tagging.feature" }, glue = {"classpath:com.cucumber.stepdefination", "classpath:com.cucumber.helper" }, format = {"pretty","html:test-output","json:json_output/cucumber.json","junit:junit_xml/cucumber.xml"},
monochrome=true,
strict=true,
dryRun=false,
tags = {"@SmokeTest , @RegressionTest"}
)


public class Runner extends AbstractTestNGCucumberTests{

}

//ORed : tags = {"@SmokeTest , @RegressionTest"} -- execute all tests tagged as @SmokeTest OR @RegressionTest
//ANDed : tags = tags = {"@SmokeTest" , "@RegressionTest"} -- execute all tests tagged as @SmokeTest AND @RegressionTest