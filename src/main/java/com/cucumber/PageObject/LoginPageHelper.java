package com.cucumber.PageObject;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.helper.WaitHelper;

import junit.framework.Assert;

public class LoginPageHelper{

	WebDriver driver;
	WaitHelper waitHelper;

	// Page Factory
	@FindBy(name = "username")
	WebElement username;

	@FindBy(name = "password")
	WebElement password;

	@FindBy(xpath = "//input[@type='submit']")
	WebElement loginBtn;
	
	//input[@type='submit' and @value='Login' and @class='btn-btn-small']

	@FindBy(xpath = "//button[contains(text(),'Sign Up']")
	WebElement signUpBtn;

	@FindBy(xpath = "//img[contains(@class,'img-responsive']")
	WebElement crmLogo;

	// Initializing the page objects
	public LoginPageHelper(WebDriver driver) {

	this.driver=driver;
    PageFactory.initElements(driver, this);
	waitHelper = new WaitHelper(driver);
		
	}

	// Actions
	public void validateLoginPageTitle() {
	String title= driver.getTitle();
	Assert.assertEquals("#1 Free CRM for Any Business: Online Customer Relationship Software", title);
	}

	
	public void login(String username, String password) {
		//this.driver=driver;
		this.username.sendKeys(username);
		this.password.sendKeys(password);
		
		}
	
	public HomePageHelper clickOnLoginButton()
	{   waitHelper.waitForElement(driver, 3000, loginBtn);
		loginBtn.click();
		return new HomePageHelper();
	}

}
