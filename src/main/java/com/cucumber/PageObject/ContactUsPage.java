package com.cucumber.PageObject;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.config.ObjectRepo;
import com.cucumber.helper.LoggerHelper;
import com.cucumber.helper.WaitHelper;

public class ContactUsPage {

//contact-link
	WebDriver driver;
	private final Logger log = LoggerHelper.getLogger(ContactUsPage.class);
	WaitHelper waitHelper;
	
	@FindBy(id="contact-link")
	WebElement contactlink;
	
	
	public ContactUsPage(WebDriver driver){
	this.driver=driver;
	PageFactory.initElements(driver, this);
	waitHelper = new WaitHelper(driver);
	waitHelper.waitForElement(driver, contactlink,ObjectRepo.reader.getExplicitWait());
	}
	
	public void clickOnContactUsLink(){
	log.info("clicked on contact us link...");
	contactlink.click();
	}
}
