package com.cucumber.PageObject;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.cucumber.config.ObjectRepo;
import com.cucumber.helper.BrowserHelper;
import com.cucumber.helper.WaitHelper;

import junit.framework.Assert;

public class HomePageHelper {

	WebDriver driver;
	WaitHelper waitHelper;
	BrowserHelper browserHelper;

	// Page Factory
	@FindBy(xpath = "//td[contains(text(), 'User : rohittest')]")
	@CacheLookup
	WebElement userNameLabel;

	@FindBy(xpath = "//a[contains(text(),'Contacts')]")
	WebElement contactsLink;

	@FindBy(xpath = "//a[contains(text(),'New Contacts')]")
	WebElement newcontactsLink;

	@FindBy(xpath = "//a[contains(text(),'Deals')]")
	WebElement dealsLink;

	@FindBy(xpath = "//a[contains(text(),'New Deal')]")
	WebElement newdealsLink;

	@FindBy(xpath = "//a[contains(text(),'Tasks')]")
	WebElement tasksLink;

	// Locator for Contact Forms
	@FindBy(id = "first_name")
	WebElement firstname;

	@FindBy(id = "surname")
	WebElement surname;

	@FindBy(id = "company_position")
	WebElement postion;

	@FindBy(xpath = "//input[@type='submit' and @value='Save']")
	WebElement saveButton;
	
	//Locator for User Deal Forms
	@FindBy(id = "title")
	WebElement title;
	
	@FindBy(id = "amount")
	WebElement amt;
	
	@FindBy(id = "probability")
	WebElement prob;
	
	@FindBy(id = "commission")
	WebElement comm;

	// Initializing the page objects
	public HomePageHelper() {

		PageFactory.initElements(driver, this);
		waitHelper = new WaitHelper(driver);
		browserHelper = new BrowserHelper(driver);
	}

	// Actions
	public String HomePageTitle() {
		return driver.getTitle();
	}

	public Boolean verifyCorrectUsername() {
		return userNameLabel.isDisplayed();
	}

	public void clickOnNewContactLinkndCreateNewContact() {
		driver.switchTo().frame("mainpanel");
		// browserHelper.switchToFrame("mainpanel");
		Actions action = new Actions(driver);
		waitHelper.setImplicitWait(60, TimeUnit.SECONDS);
		action.moveToElement(contactsLink).build().perform();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", contactsLink);
	}

	public void fillUserForm(String firstname, String lastname, String position) {
		waitHelper.setImplicitWait(60, TimeUnit.SECONDS);
		this.firstname.sendKeys(firstname);
		this.surname.sendKeys(lastname);
		this.postion.sendKeys(position);
		this.saveButton.click();
	}

	public void verifyHomeTitle() {
		String hometitle = driver.getTitle();
		Assert.assertEquals("CRMPRO", hometitle);
	}

	public void movestoDealPage() {
		driver.switchTo().frame("mainpanel");
		Actions action = new Actions(driver);
		action.moveToElement(dealsLink).build().perform();
		waitHelper.setImplicitWait(60, TimeUnit.SECONDS);
		newdealsLink.click();
	}
	
	public void fillNewDealsForm(String title, String amount, String probability, String commision)
	{
		this.title.sendKeys(title);
		this.amt.sendKeys(amount);
		this.prob.sendKeys(probability);
		this.comm.sendKeys(commision);
		saveButton.click();
	}

}
