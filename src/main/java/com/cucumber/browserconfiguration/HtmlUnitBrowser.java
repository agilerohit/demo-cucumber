
package com.cucumber.browserconfiguration;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class HtmlUnitBrowser {
	
	public HtmlUnitBrowser(Capabilities cap) {
		// TODO Auto-generated constructor stub
	}

	public Capabilities getHtmlUnitDriverCapabilities() {
		DesiredCapabilities unit = DesiredCapabilities.htmlUnit();
		return unit;
	}
	
	public WebDriver getHtmlUnitDriver(Capabilities cap) {
		return (WebDriver) new HtmlUnitBrowser(cap);
	}

}
