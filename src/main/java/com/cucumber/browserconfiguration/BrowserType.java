
package com.cucumber.browserconfiguration;
/**
 * 
 * @author rohit
 *
 */
public enum BrowserType {
	Firefox,
	Iexplorer,
	HtmlUnitDriver,
	Chrome
}
